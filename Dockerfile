FROM node:10

COPY package*.json ./

RUN npm install 

COPY . .

EXPOSE 8503

CMD ["npm", "start"]
