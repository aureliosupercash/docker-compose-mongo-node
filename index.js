var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var app = express();

/* GET home page. */
app.get('/', function(req, res, next) {
  mongoose.connect('mongodb://mongot:27017/my_database');
  MyModel.findOne(function(error, result) { 
    res.render('index', { title: result.text });
  });
});

app.listen(8503, function() {
    console.log("Listening");
});
